package net.e621.e6l

import net.e621.e6l.Platform.FSAccess
import net.e621.e6l.Platform.GUILibrary
import net.e621.e6l.Platform.JSONImplementation
import net.e621.e6l.Platform.WebClient
import net.e621.e6l.jvm.JVMActualGUI

object Main {
	lateinit var platformFS: FSAccess
//	lateinit var platformGUI: GUILibrary
	var platformGUI: GUILibrary = JVMActualGUI
	lateinit var platformJSON: JSONImplementation
	lateinit var platformWebClient: WebClient
	@JvmStatic fun main(args: Array<String>) = platformGUI.initAndDrawMain(args)
	//TODO don't use RuntimeException from java
	fun setImplementations(targetName: String, fsAccess: FSAccess, guiLibrary: GUILibrary,
			jsonImplementation: JSONImplementation, webClient: WebClient) {
		platformFS = fsAccess
		platformGUI = guiLibrary
		platformJSON = jsonImplementation
		platformWebClient = webClient.withUA(targetName)
	}
}
