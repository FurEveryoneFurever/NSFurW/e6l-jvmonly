package net.e621.e6l.jvm.e6_tabbed

import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TextField
import net.e621.e6l.jvm.e6_tabbed.ESixTabbedController
import tornadofx.*

class PostsTabView: View() {
	private val controller: ESixTabbedController by inject()
	private val input = SimpleStringProperty()
	lateinit var textfield: TextField
	override val root = vbox {
		hbox {
			button("List")
			button("Recent Changes")
			button("Help")
		}
		hbox {
			vbox {
				form {
					fieldset {
						hbox {
							button("Show tags of post: #") {
								action {
									runAsyncWithProgress {
										controller.postList1(input.value.toInt())
									} ui { loadedText ->
										textfield.text = loadedText
										input.value = ""
									}
								}
							}
							field { textfield(input) }
						}
					}
				}
				//TODO tag list
			}
			textfield = textfield()
			//TODO post thumbnails
		}
	}
}
