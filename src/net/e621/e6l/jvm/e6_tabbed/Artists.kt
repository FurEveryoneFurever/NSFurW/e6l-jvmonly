package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class ArtistsTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("Recent Changes")
			button("Help")
		}
		hbox {
			label("TODO Artists")
		}
	}
}
