package net.e621.e6l.jvm

import com.beust.klaxon.*
import net.e621.e6l.ESixAPI.DTextString
import net.e621.e6l.ESixAPI.DataModifiedResponse
import net.e621.e6l.ESixAPI.Posts.FFDData
import net.e621.e6l.ESixAPI.Posts.InternalPost
import net.e621.e6l.ESixAPI.Posts.PostExistanceStatus
import net.e621.e6l.ESixAPI.Posts.PostUploadResponse
import net.e621.e6l.ESixAPI.Posts.RatingType
import net.e621.e6l.ESixAPI.Tags.InternalTag
import net.e621.e6l.ESixAPI.Tags.TagType
import net.e621.e6l.ESixAPI.Wiki.WikiPage
import net.e621.e6l.Platform.JSONImplementation
import java.io.StringReader
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import kotlin.reflect.KClass

object JVMActualJSON: JSONImplementation {
	private class ParseException(parsedAs: KClass<*>):
		RuntimeException("failed to parse JSON as instance of ${parsedAs.qualifiedName!!}")

	@Target(AnnotationTarget.FIELD) private annotation class JSONDecodedE6DText
	private object ESixDTextConverter: Converter {
		override fun canConvert(cls: Class<*>) = cls == DTextString::class.java
		override fun fromJson(jv: JsonValue) = jv.string?.let { DTextString(it) }
			?: throw KlaxonException("failed to parse rating: ${jv.string}")
		override fun toJson(value: Any) = (value as DTextString).rawString
	}

	@Target(AnnotationTarget.FIELD) private annotation class JSONDecodedE6Rating
	private object ESixRatingConverter: Converter {
		override fun canConvert(cls: Class<*>) = cls == RatingType::class.java
		override fun fromJson(jv: JsonValue) = jv.string?.let { RatingType.parse(it) }
			?: throw KlaxonException("failed to parse rating: ${jv.string}")
		override fun toJson(value: Any) = (value as RatingType).jsonForm
	}

	@Target(AnnotationTarget.FIELD) private annotation class JSONDecodedE6TagType
	private object ESixTagTypeConverter: Converter {
		override fun canConvert(cls: Class<*>) = cls == TagType::class.java
		override fun fromJson(jv: JsonValue) = jv.int?.let { TagType.parse(it) }
			?: throw KlaxonException("failed to parse tag type: ${jv.int}")
		override fun toJson(value: Any) = (value as TagType).jsonForm.toString()
	}

	@Target(AnnotationTarget.FIELD) private annotation class JSONDecodedTime
	private object JSTypedTimeConverter: Converter {
		override fun canConvert(cls: Class<*>) = cls == LocalDateTime::class.java
		override fun fromJson(jv: JsonValue) = jv.obj
			?.let { LocalDateTime.ofEpochSecond((it["s"] as Int).toLong(), it["n"] as Int, ZoneOffset.UTC) }
			?: throw KlaxonException("failed to parse timestamp: ${jv.obj}")
		override fun toJson(value: Any) = (value as LocalDateTime).run {
			"{\"json_class\":\"Time\",\"s\":${toEpochSecond(ZoneOffset.UTC)},\"n\":$nano}" }
	}

	@Target(AnnotationTarget.FIELD) private annotation class JSONDecodedUnixSeconds
	private object UnixSecondsConverter: Converter {
		override fun canConvert(cls: Class<*>) = cls == Instant::class.java
		override fun fromJson(jv: JsonValue) = jv.longValue?.let { Instant.ofEpochSecond(it) }
			?: throw KlaxonException("failed to parse timestamp: ${jv.longValue}")
		override fun toJson(value: Any) = (value as Instant).epochSecond.toString()
	}

	private val JSON_PARSER by lazy { Klaxon() }

	override fun parseDataModifiedResponse(jsonString: String) = JSON_PARSER.parse<DataModifiedResponse>(jsonString)
		?: throw ParseException(DataModifiedResponse::class)

	private val FFD_DATA_PARSER by lazy { JSON_PARSER
		.fieldConverter(JSONDecodedUnixSeconds::class, UnixSecondsConverter) }
	override fun parseFFDList(jsonString: String): List<FFDData>? {
		val result = mutableListOf<FFDData>()
		JsonReader(StringReader(jsonString)).use { reader -> reader.beginArray { while (reader.hasNext())
			result.add(FFD_DATA_PARSER.parse<FFDData>(reader) ?: throw ParseException(FFDData::class))
		} }
		return result
	}

	@Suppress("ArrayInDataClass") private data class KlaxonInternalPost(val artist: Array<String>, val author: String,
			val change: Int, val children: String, @JSONDecodedTime val created_at: LocalDateTime, val creator_id: Int,
			val description: String, val fav_count: Int, val file_ext: String, val file_size: Int, val file_url: String,
			val has_children: Boolean, val has_comments: Boolean, val has_notes: Boolean, val height: Int, val id: Int,
			val md5: String, val preview_height: Int, val preview_width: Int, val preview_url: String,
			@JSONDecodedE6Rating val rating: RatingType, val sample_height: Int, val sample_width: Int,
			val sample_url: String, val score: Int, val source: String, val sources: Array<String>, val status: String,
			val tags: String, val width: Int, val locked_tags: String? = null, val parent_id: Int? = null) {
		fun asInternalPost() = InternalPost(id, change, status, tags, created_at, creator_id, author, source,
			sources, file_ext, file_size, md5, file_url, width, height, preview_url, preview_width, preview_height,
			sample_url, sample_width, sample_height, rating, artist, has_notes, description, score, fav_count,
			has_comments, has_children, children, parent_id, locked_tags)
	}
	private val POST_DATA_PARSER by lazy { JSON_PARSER
		.fieldConverter(JSONDecodedE6Rating::class, ESixRatingConverter)
		.fieldConverter(JSONDecodedTime::class, JSTypedTimeConverter) }
	override fun parseInternalPost(jsonString: String) =
		POST_DATA_PARSER.parse<KlaxonInternalPost>(jsonString)?.asInternalPost()
			?: throw ParseException(KlaxonInternalPost::class)
	override fun parseInternalPostList(jsonString: String): List<InternalPost>? {
		val result = mutableListOf<KlaxonInternalPost>()
		JsonReader(StringReader(jsonString)).use { reader -> reader.beginArray { while (reader.hasNext())
			result.add(POST_DATA_PARSER.parse<KlaxonInternalPost>(reader)
				?: throw ParseException(KlaxonInternalPost::class))
		} }
		return result.map { it.asInternalPost() }
	}

	private val TAG_DATA_PARSER by lazy { JSON_PARSER
		.fieldConverter(JSONDecodedE6TagType::class, ESixTagTypeConverter) }
	override fun parseInternalTag(jsonString: String) = TAG_DATA_PARSER.parse<InternalTag>(jsonString)
		?: throw ParseException(InternalTag::class)
	override fun parseInternalTagList(jsonString: String): MutableList<InternalTag>? {
		val result = mutableListOf<InternalTag>()
		JsonReader(StringReader(jsonString)).use { reader -> reader.beginArray { while (reader.hasNext())
			result.add(TAG_DATA_PARSER.parse<InternalTag>(reader)
				?: throw ParseException(InternalTag::class))
		} }
		return result
	}

	override fun parseInternalTagAlias(jsonString: String) = TODO()

	override fun parsePostExistanceStatus(jsonString: String) = JSON_PARSER.parse<PostExistanceStatus>(jsonString)
		?: throw ParseException(PostExistanceStatus::class)

	override fun parsePostUploadResponse(jsonString: String) = JSON_PARSER.parse<PostUploadResponse>(jsonString)
		?: throw ParseException(PostUploadResponse::class)

	private val WIKI_PAGE_PARSER by lazy { JSON_PARSER
		.fieldConverter(JSONDecodedE6DText::class, ESixDTextConverter)
		.fieldConverter(JSONDecodedTime::class, JSTypedTimeConverter) }
	override fun parseWikiPage(jsonString: String) = WIKI_PAGE_PARSER.parse<WikiPage>(jsonString)
		?: throw ParseException(WikiPage::class)
}
