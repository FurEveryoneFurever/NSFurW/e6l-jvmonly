package net.e621.e6l.jvm

import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import net.e621.e6l.jvm.e6_tabbed.ESixTabbedView
import tornadofx.*

class E6lMasterController: Controller() {
	fun exit() = app.workspace.close()
	fun saveAndExit() {
		//TODO
		exit()
	}
}

class E6lMasterView: View() {
	private val assignedKeys = mutableSetOf<KeyCode>()
	private fun shortcutFrom(key: KeyCode): KeyCodeCombination {
		if (assignedKeys.contains(key)) throw IllegalArgumentException("cannot assign key to multiple menu actions")
		assignedKeys.add(key)
		return KeyCodeCombination(key, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN)
	}
	private val controller: E6lMasterController by inject()
	override val root = borderpane {
		top {
			menubar {
				menu("File") {
					menu("Last viewed post") {
						item("Copy post link")
						item("Copy direct link")
						item("Save as...")
						item("Print...")
					}
					item("Preferences...", shortcutFrom(KeyCode.P))
					separator()
					item("Exit (prompt to save)")
						.action { controller.saveAndExit() }
					item("Exit now (discard all)", KeyCodeCombination(KeyCode.F4, KeyCombination.ALT_DOWN))
						.action { controller.exit() }
				}
				menu("Edit") {
					item("Cancel pending action", shortcutFrom(KeyCode.C))
					item("Undo last action...", shortcutFrom(KeyCode.Z))
				}
				menu("View") {
					menu("Use layout") {
						radiomenuitem("eSix Tabbed (e621.net clone)").isSelected = true
					}
					separator()
					menu("Coarse filter") {
						radiomenuitem("Definitely safe (disables most features)",
							keyCombination = shortcutFrom(KeyCode.S))
						radiomenuitem("Not marked questionable or explicit").isSelected = true
						radiomenuitem("Not marked explicit")
						radiomenuitem("Everything")
						radiomenuitem("Questionable and explicit only")
					}
					menu("Enforce blacklists") {
						checkmenuitem("Local")
						checkmenuitem("Suggested")
					}
					separator()
					item("Panic", shortcutFrom(KeyCode.SPACE))
				}
				menu("Web") {
					checkmenuitem("Block all networking?", shortcutFrom(KeyCode.N))
					menu("Clear cache of") {
						item("Anything not used this month")
						item("Anything not used this week")
						item("Thumbnails (+ eSix \"previews\")")
						item("All images")
						item("All images and text")
					}
					menu("Accounts") {
						item("Log in to eSix...")
						item("Log in to FurAffinity...")
						item("Log in to Reddit...")
					}
				}
				menu("Help") {
					checkmenuitem("Show protip on launch?").isSelected = true
					item("Changelog...")
					item("User manual @ GitLab")
					item("Quickstart guide...")
					separator()
					item("Request changes @ GitLab")
					item("Report a bug @ GitLab")
					item("Source code @ GitLab")
					separator()
					item("About")
				}
			}
		}
		center<ESixTabbedView>()
		bottom {
			//TODO status bar
		}
	}
}
