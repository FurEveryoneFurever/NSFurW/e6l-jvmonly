package net.e621.e6l.jvm.e6_tabbed

import tornadofx.*

class TagsTabView: View() {
	override val root = vbox {
		hbox {
			button("List")
			button("Aliases")
			button("Implications")
			button("Help")
		}
		hbox {
			label("TODO Tags")
		}
	}
}
