package net.e621.e6l.jvm

import net.e621.e6l.Platform.FSAccess
import net.e621.e6l.Platform.FSAccess.Companion.LocalFile
import net.e621.e6l.Platform.FSAccess.Companion.MD5Checksum

object JVMActualFS: FSAccess {
	private class JVMFile: LocalFile {
		override val md5 by lazy { MD5Checksum(TODO("call md5sum or similar")) }
	}
}
