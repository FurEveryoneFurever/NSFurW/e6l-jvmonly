@file:Suppress("unused", "UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")

package net.e621.e6l

import net.e621.e6l.ESixAPI.Users.UserIDPair
import net.e621.e6l.Main.platformJSON
import net.e621.e6l.Main.platformWebClient
import net.e621.e6l.Platform.FSAccess.Companion.LocalFile
import net.e621.e6l.Platform.FSAccess.Companion.MD5Checksum
import java.time.Duration
import java.time.LocalDateTime

object ESixAPI {
	private const val INVALID_ID = -999

	data class DTextString(val rawString: String)

	object Implications {
		private const val LIST_ALIASES_ACTION = "tag_alias/index"
		data class InternalTagAlias(val id: Int, val name: String, val alias_id: Int, val pending: Boolean)
		fun listAliases(page: Int = 1) = get(LIST_ALIASES_ACTION, mapOf("page" to page))
			?.let { platformJSON.parseInternalTagAlias(it) }
	}

	object Posts {
		private const val EXISTS_ACTION = "post/check_md5"
		private const val FLAG_ACTION = "post/flag"
		private const val LIST_ACTION = "post/index"
		private const val LIST_FFDS_ACTION = "post_flag_history/index"
		private const val LIST1_ACTION = "post/show"
		private const val MODIFY_ACTION = "post/update"
		private const val TAGS_ACTION = "post/tags"
		private const val UPLOAD_ACTION = "post/create"
		data class CompoundFFDReason(val reason: FFDReason)
		data class FFDData(val id: Int, val post_id: Int, val created_at: Duration, val reason: String, val user_id: Int)
		enum class FFDReason(val jsonForm: String) {
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Uploader requests deletion* */
			Misupload("uploader"),
			/**
			 * From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Repost/inferior version of existing
			 * post*
			 *
			 * The eSix FFD process doesn't differentiate between duplicate and inferior posts, so in UI that does, both
			 * options will be functionally equivalent. In practice, the later post is deleted in the case of a
			 * "duplicate" and the earlier in the case of an "inferior" FFD, and upgrading old posts to higher
			 * resolutions seems to be more common than users posting the same image with a different checksum.
			 * @see Inferior
			 */
			Duplicate("inferior"),
			/** @see Duplicate */
			Inferior("inferior"),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Artist is on avoid-posting list* */
			DNPArtist(1),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Post is paysite material* */
			NonGratisContent(2),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Uncredited trace* */
			UncreditedTrace(3),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Real-life pornography* */
			RealLifeNSFW(4),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *File corrupted* */
			BrokenFile(5),
			/** From the [docs](https://e621.net/wiki/show/e621:api#posts_flag): *Image previously deleted* */
			RepostOfDeleted(6),
			INVALID(INVALID_ID);
			constructor(jsonForm: Int): this(jsonForm.toString())
		}
		data class InternalPost(val id: Int, val change: Int, val status: String, val tags: String,
				val created_at: LocalDateTime, val creator_id: Int, val author: String,
				val source: String, val sources: Array<String>,
				val file_ext: String, val file_size: Int, val md5: String,
				val file_url: String, val width: Int, val height: Int,
				val preview_url: String, val preview_width: Int, val preview_height: Int,
				val sample_url: String, val sample_width: Int, val sample_height: Int,
				val rating: ESixAPI.Posts.RatingType, val artist: Array<String>,
				val has_notes: Boolean, val description: String,
				val score: Int, val fav_count: Int, val has_comments: Boolean,
				val has_children: Boolean, val children: String,
				val parent_id: Int? = null, val locked_tags: String? = null) {
			override fun equals(other: Any?) = this === other || (other as? InternalPost)?.let {
				id == it.id && change == it.change && parent_id == it.parent_id && has_notes == it.has_notes &&
					score == it.score && fav_count == it.fav_count && has_comments == it.has_comments } ?: false
			override fun hashCode(): Int {
				var result = id
				arrayOf(change, parent_id ?: -1, has_notes.hashCode(), score, fav_count, has_comments.hashCode())
					.forEach { result = 31 * result + it }
				return result
			}
			fun toPostData() = PostData(id, PostCreationInfo(created_at, UserIDPair(creator_id, author)))
		}
		data class PostCreationInfo(val timestamp: LocalDateTime, val uploader: UserIDPair)
		class PostData(override val id: Int, val creationInfo: PostCreationInfo): PostIDOrData {
			override fun withData() = this
		}
		class PostDiff(post: InternalPost) { //TODO strip revision, uris, etc. as separate class
			class NoOpModify: IllegalArgumentException(NOOP_MODIFY) {
				companion object {
					private const val NOOP_MODIFY = "no-op post modification ignored"
				}
			}
			fun asPostMap() = emptyMap<String, Any>()
		}
		data class PostExistanceStatus(val exists: Boolean, val post_id: Int? = null)
		data class PostID(override val id: Int): PostIDOrData {
			override fun withData() = list1(id)!!.toPostData()
		}
		interface PostIDOrData {
			val id: Int
			fun withData(): PostData
		}
		class PostSource(private val uris: List<String>) {
			companion object {
				const val WRONG_URI_COUNT = "sources must have between 1 and 5 URIs"
			}
			init {
				if (uris.size !in 1..5) throw IllegalArgumentException(WRONG_URI_COUNT)
			}
			constructor(uri: String): this(listOf(uri))
			override fun toString() = uris.joinToString(" ")
		}
		class PostUploadResponse(success: Boolean, reason: String? = null, location: String? = null): DataModifiedResponse(success, reason) {
			companion object {
				/**
				 * From the [docs](https://e926.net/wiki/show/e621:api#posts_create): *This means you supplied an MD5
				 * parameter and what e621 got doesn't match. Try uploading the file again.*
				 */
				class ChecksumMismatchException: RuntimeException("eSix received a file with a different checksum to the one calculated, most likely due to a poor connection")
				/**
				 * From the [docs](https://e926.net/wiki/show/e621:api#posts_create): *This post already exists in e621
				 * (based on the MD5 hash). An additional attribute called location will be set, pointing to the
				 * (relative) URL of the original post.*
				 */
				class DuplicateUploadException(location: String): RuntimeException("That file already exists at e621.net$location (that, or you've found an MD5 collision)")
				class UnknownUploadException(reason: String): RuntimeException(reason)
			}
			override val success: Boolean = when (reason) {
				"MD5 mismatch" -> throw ChecksumMismatchException()
				"duplicate" -> throw DuplicateUploadException(location!!)
				else -> if (success) success else throw UnknownUploadException(reason!!)
			}
		}
		enum class RatingType(val jsonForm: String) {
			Safe("s"), Questionable("q"), Explicit("e"),
			INVALID(INVALID_ID.toString());
			companion object {
				private val idMap by lazy { mapOf("s" to Safe, "q" to Questionable, "e" to Explicit) }
				fun parse(typeID: String) = idMap[typeID] ?: INVALID
			}
		}

		fun createFromURI(uri: String, tags: String, rating: RatingType, source: PostSource = PostSource(uri),
				description: String? = null, lockRating: Boolean? = null, lockNotes: Boolean? = null, parentID: Int? = null) =
			post(UPLOAD_ACTION, mutableMapOf("tags" to tags, "rating" to rating, "upload_url" to uri, "source" to source.toString())
					.also {
						description?.run { it["description"] = this }
						lockRating?.run { it["is_rating_locked"] = this }
						lockNotes?.run { it["is_note_locked"] = this }
						parentID?.run { it["parent_id"] = this } })
				?.let { platformJSON.parsePostUploadResponse(it) }
		fun flag(postID: PostID, reason: FFDReason) =
			post(FLAG_ACTION, mapOf("id" to postID.id, "flag_option" to reason.jsonForm))
				?.let { platformJSON.parseDataModifiedResponse(it) }
		fun getExistanceOf(md5: MD5Checksum) = get(EXISTS_ACTION, mapOf("md5" to md5))
			?.let { platformJSON.parsePostExistanceStatus(it) }
		fun getPastFFDsOf(postID: PostID) = get(LIST_FFDS_ACTION, mapOf("id" to postID.id))
			?.let { platformJSON.parseFFDList(it) }
		fun getTagsOf(postID: PostID) = get(TAGS_ACTION, mapOf("id" to postID.id))
			?.let { platformJSON.parseInternalTagList(it) } //TODO
		fun getTagsOf(md5: MD5Checksum) = get(TAGS_ACTION, mapOf("md5" to md5))
			?.let { platformJSON.parseInternalTagList(it) } //TODO
		fun list(tags: String, postedBeforeID: Int, limit: Int = 320, typed_tags: Boolean = false) =
			get(LIST_ACTION, mapOf("limit" to limit, "before_id" to postedBeforeID, "tags" to tags, "typed_tags" to typed_tags))
				?.let { platformJSON.parseInternalPostList(it) }
		fun list(tags: String, limit: Int = 320, typed_tags: Boolean = false) =
			get(LIST_ACTION, mapOf("limit" to limit, "tags" to tags, "typed_tags" to typed_tags))
				?.let { platformJSON.parseInternalPostList(it) }
		fun list1(id: Int) = get(LIST1_ACTION, mapOf("id" to id))?.let { platformJSON.parseInternalPost(it) }
		fun list1(md5: MD5Checksum) = get(LIST1_ACTION, mapOf("md5" to md5.hexString))
			?.let { platformJSON.parseInternalPost(it) }
		fun modify(post: PostIDOrData, diff: PostDiff, reason: String? = null) =
			post(MODIFY_ACTION, diff.asPostMap().toMutableMap().also {
					it["id"] = post.id
					reason?.run { it["reason"] = this }
				})?.let { platformJSON.parseDataModifiedResponse(it) }
		//TODO to PaginatedPostList() w/ getPage() function
		fun paginatedList(tags: String, limit: Int = 320, page: Int = 0, typed_tags: Boolean = false) =
			get(LIST_ACTION, mapOf("limit" to limit, "page" to page, "tags" to tags, "typed_tags" to typed_tags))
				?.let { platformJSON.parseInternalPostList(it) }
		fun upload(file: LocalFile, tags: String, rating: RatingType, source: PostSource,
				description: String? = null, lockRating: Boolean? = null, lockNotes: Boolean? = null, parentID: Int? = null) =
			post(UPLOAD_ACTION, mutableMapOf("tags" to tags, "file" to file, "md5" to file.md5, "rating" to rating, "source" to source.toString())
					.also {
						description?.run { it["description"] = this }
						lockRating?.run { it["is_rating_locked"] = this }
						lockNotes?.run { it["is_note_locked"] = this }
						parentID?.run { it["parent_id"] = this } })
				?.let { platformJSON.parsePostUploadResponse(it) }
	}

	object Tags {
		private const val LIST_ACTION = "tag/list"
		private const val LIST1_ACTION = "tag/show"
		private const val MODIFY_ACTION = "tag/update"
		private const val RELATED_ACTION = "tag/related"
		class InternalTag(val id: Int, val name: String, val count: Int, val type: TagType)
		class InvalidTagIDException(id: Int): RuntimeException("no tag with ID $id")
		enum class TagListOrder(val jsonForm: String)
		enum class TagType(val jsonForm: Int, val stringForm: String, val shortForm: String) {
			General(0, "general", "gen"), Artist(1, "artist", "art"),
			REDACTED(2, "???", "?"), Copyright(3, "copyright", "copy"),
			Character(4, "character", "char"), Species(5, "species", "species"),
			INVALID(INVALID_ID, "invalid", "invd");
			companion object {
				private val idMap =
					mapOf(0 to General, 1 to Artist, 2 to REDACTED, 3 to Copyright, 4 to Character,5 to Species)
				fun parse(typeID: Int) = idMap[typeID] ?: INVALID
			}
		}

		fun list(name: String, order: TagListOrder, limit: Int = 500) =
			get(LIST_ACTION, mapOf("name" to name, "order" to order.jsonForm, "limit" to limit))
				?.let { platformJSON.parseInternalTagList(it) }
		fun list(namePattern: String) = get(LIST_ACTION, mapOf("name_pattern" to namePattern))
			?.let { platformJSON.parseInternalTagList(it) }
		fun list1(id: Int) = get(LIST1_ACTION, mapOf("id" to id))?.let { platformJSON.parseInternalTag(it) }
		fun modify(name: String, type: TagType, isAmbiguous: Boolean? = null) =
			post(LIST1_ACTION, mutableMapOf("name" to name, "type" to type.jsonForm)
					.also { isAmbiguous?.run { it["is_ambiguous"] = this } })
				?.let { platformJSON.parseDataModifiedResponse(it) }
		fun modify(id: Int, type: TagType, isAmbiguous: Boolean? = null) =
			list1(id)?.name?.let { modify(it, type, isAmbiguous) } ?: throw InvalidTagIDException(id)
		fun related(tags: List<String>, type: TagType? = null) =
			get(RELATED_ACTION, mutableMapOf("tags" to tags.joinToString(" "))
					.also { type?.run { it["type"] = this.jsonForm.toString() } })
				?.let { platformJSON.parseInternalTagList(it) }
	}

	object Users {
		data class UserData(override val id: Int, override val name: String): UserIDPairOrData {
			override fun withData() = this
		}
		data class UserIDPair(override val id: Int, override val name: String): UserIDPairOrData {
			override fun withData() = list1(id)
		}
		interface UserIDPairOrData {
			val id: Int
			val name: String
			fun withData(): UserData
		}

		fun list1(id: Int): UserData = TODO()
	}

	object Wiki {
		private const val CHANGES_ACTION = "wiki/recent_changes"
		private const val CREATE_ACTION = "wiki/create"
		private const val DESTROY_ACTION = "wiki/destroy"
		private const val HISTORY_ACTION = "wiki/history"
		private const val LIST_ACTION = "wiki/index"
		private const val LIST1_ACTION = "wiki/show"
		private const val LOCK_ACTION = "wiki/lock"
		private const val REVERT_ACTION = "wiki/revert"
		private const val UNLOCK_ACTION = "wiki/unlock"
		private const val UPDATE_ACTION = "wiki/update"
		data class WikiPage(val id: Int, val created_at: LocalDateTime, val updated_at: LocalDateTime,
			val title: String, val body: DTextString, val updater_id: Int, val locked: Boolean, val version: Int)
		fun list1(title: String) = get(LIST1_ACTION, mapOf("title" to title))?.let { platformJSON.parseWikiPage(it) }
	}

	//TODO check nullability of reason
	open class DataModifiedResponse(open val success: Boolean, val reason: String? = null)
//	private fun get(action: String, params: Map<String, Any>? = null, useSpecial: Boolean = false) =
//		platformWebClient.get("https://e${if (useSpecial) 621 else 926}.net/$action.json", params).body
//	private fun post(action: String, params: Map<String, Any>? = null, useSpecial: Boolean = false) =
//		platformWebClient.post("https://e${if (useSpecial) 621 else 926}.net/$action.json", params).body
	private fun get(action: String, params: Map<String, Any>? = null, useSpecial: Boolean = false) =
		platformWebClient.get("https://e621.net/$action.json", params).body
	private fun post(action: String, params: Map<String, Any>? = null, useSpecial: Boolean = false) =
		platformWebClient.post("https://e621.net/$action.json", params).body
}
